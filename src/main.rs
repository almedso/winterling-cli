#[macro_use]
extern crate log;
extern crate simplelog;
extern crate clap;

use clap::{Arg, App,  SubCommand};
use simplelog::*;

mod commands;

fn main() {
    let matches = App::new("Winterling CLI")
        .version("0.1.0")
        .author("volker.kempert@almedso.de>")
        .about("Does awesome things")
        .arg(Arg::with_name("URL")
            .short("u")
            .long("url")
            .value_name("url")
            .help("Set a base URL of the winterling service")
            .takes_value(true))
        .arg(Arg::with_name("v")
            .short("v")
            .multiple(true)
            .help("Sets the level of verbosity"))
        .subcommand(commands::test::declare())
        .get_matches();

    // Vary the output based on how many times the user used the "verbose" flag
    let min_log_level = match matches.occurrences_of("v") {
        0 => LevelFilter::Info,
        1 => LevelFilter::Debug,
        2 | _ => LevelFilter::Trace,
    };
    TermLogger::init(min_log_level, Config::default(), TerminalMode::Mixed).unwrap();
    let url = matches.value_of("URL").unwrap_or("http://localhost:4242");
    debug!("Access service at: {}", url);

    commands::test::process(&matches, &url);
}
