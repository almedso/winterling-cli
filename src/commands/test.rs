extern crate log;
extern crate simplelog;
extern crate clap;

use clap::{Arg, App, SubCommand, ArgMatches};


pub fn declare<'a,'b>() -> App<'a,'b> {
    SubCommand::with_name("test")
        .about("controls testing features")
        .version("0.1.0")
        .author("volker.kempert@almedso.de>")
        .arg(Arg::with_name("debug")
            .short("d")
            .help("print debug information verbosely"))
}


pub fn process<'a, 'b>(matches: & ArgMatches<'a>, url: &'b str) {
    // You can handle information about subcommands by requesting their matches by name
    // (as below), requesting just the name used, or both at the same time
    if let Some(matches) = matches.subcommand_matches("test") {
        if matches.is_present("debug") {
            debug!("Found debug flag -- test");
            debug!("Url is: {}", url);
        } else {
            info!("Found info flag -- test ");
        }
    }
}

